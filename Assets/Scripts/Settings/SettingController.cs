﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingController : MonoBehaviour
{
    HTTPWebRequest hTTPWebRequest;

    UIManager uIManager;


    void Start()
    {
        hTTPWebRequest = FindObjectOfType<HTTPWebRequest>();
        uIManager = FindObjectOfType<UIManager>();
    }

    void Update()
    {
        
    }

    public void _Logout()
    {
        hTTPWebRequest._LogoutCoroutine();

        uIManager.taskbarButtonController._SettingButtonClick();
    }
}
