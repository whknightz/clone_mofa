﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPanelsController : MonoBehaviour
{
    public AllUsersPanel allUsersPanel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void _CloseAllPanels()
    {
        allUsersPanel.gameObject.SetActive(false);
    }

    public void _OpenAllUserPanel()
    {
        allUsersPanel.gameObject.SetActive(true);
    }
}
