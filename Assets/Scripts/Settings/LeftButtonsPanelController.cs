﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftButtonsPanelController : MonoBehaviour
{
    public enum CLICK_BUTTON { None, AllUser, UserGroup, Security, PreAlarm, MainAlarm, Interval, Generaly, Function }

    public GameObject allUserButton, allUserButtonActive;
    public GameObject userGroupButton, userGroupButtonActive;
    public GameObject securityButton, securityButtonActive;
    public GameObject preAlarmButton, preAlarmButtonActive;
    public GameObject mainAlarmButton, mainAlarmButtonActive;
    public GameObject intervalButton, intervalButtonActive;
    public GameObject generalyButton, generalyButtonActive;
    public GameObject functionButton, functionButtonActive;

    public CLICK_BUTTON currentClick = CLICK_BUTTON.None;

    // Start is called before the first frame update
    void Start()
    {
        //_OutTask();
        _AllUserButtonClick();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void _OutTask()
    {
        _ActiveGreyButton();
        _DeactiveRedButton();

        currentClick = CLICK_BUTTON.None;
    }

    void _ActiveGreyButton()
    {
        allUserButton.SetActive(true);
        userGroupButton.SetActive(true);
        securityButton.SetActive(true);
        preAlarmButton.SetActive(true);
        mainAlarmButton.SetActive(true);
        intervalButton.SetActive(true);
        generalyButton.SetActive(true);
        functionButton.SetActive(true);
    }

    void _DeactiveRedButton()
    {
        allUserButtonActive.SetActive(false);
        userGroupButtonActive.SetActive(false);
        securityButtonActive.SetActive(false);
        preAlarmButtonActive.SetActive(false);
        mainAlarmButtonActive.SetActive(false);
        intervalButtonActive.SetActive(false);
        generalyButtonActive.SetActive(false);
        functionButtonActive.SetActive(false);
    }

    public void _AllUserButtonClick()
    {
        if (currentClick != CLICK_BUTTON.AllUser)
        {
            currentClick = CLICK_BUTTON.AllUser;

            _ActiveGreyButton();
            _DeactiveRedButton();

            allUserButton.SetActive(false);
            allUserButtonActive.SetActive(true);
        }
        else
        {
            _OutTask();
        }
    }

    public void _UserGroupButtonClick()
    {
        if (currentClick != CLICK_BUTTON.UserGroup)
        {
            currentClick = CLICK_BUTTON.UserGroup;

            _ActiveGreyButton();
            _DeactiveRedButton();

            userGroupButton.SetActive(false);
            userGroupButtonActive.SetActive(true);
        }
        else
        {
            _OutTask();
        }
    }

    public void _SecurityButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Security)
        {
            currentClick = CLICK_BUTTON.Security;

            _ActiveGreyButton();
            _DeactiveRedButton();

            securityButton.SetActive(false);
            securityButtonActive.SetActive(true);
        }
        else
        {
            _OutTask();
        }
    }

    public void _PreAlarmButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.PreAlarm)
        {
            currentClick = CLICK_BUTTON.PreAlarm;

            _ActiveGreyButton();
            _DeactiveRedButton();

            preAlarmButton.SetActive(false);
            preAlarmButtonActive.SetActive(true);
        }
        else
        {
            _OutTask();
        }
    }

    public void _MainAlarmButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.MainAlarm)
        {
            currentClick = CLICK_BUTTON.MainAlarm;

            _ActiveGreyButton();
            _DeactiveRedButton();

            mainAlarmButton.SetActive(false);
            mainAlarmButtonActive.SetActive(true);
        }
        else
        {
            _OutTask();
        }
    }

    public void _IntervalButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Interval)
        {
            currentClick = CLICK_BUTTON.Interval;

            _ActiveGreyButton();
            _DeactiveRedButton();

            intervalButton.SetActive(false);
            intervalButtonActive.SetActive(true);
        }
        else
        {
            _OutTask();
        }
    }

    public void _GeneralyButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Generaly)
        {
            currentClick = CLICK_BUTTON.Generaly;

            _ActiveGreyButton();
            _DeactiveRedButton();

            generalyButton.SetActive(false);
            generalyButtonActive.SetActive(true);
        }
        else
        {
            _OutTask();
        }
    }

    public void _FunctionButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Function)
        {
            currentClick = CLICK_BUTTON.Function;

            _ActiveGreyButton();
            _DeactiveRedButton();

            functionButton.SetActive(false);
            functionButtonActive.SetActive(true);
        }
        else
        {
            _OutTask();
        }
    }
}
