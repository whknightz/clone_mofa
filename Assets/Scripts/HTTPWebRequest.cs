﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

public class HTTPWebRequest : MonoBehaviour
{
    UIManager uIManager;

    string loginUrl = "http://27.72.147.222:5011/api/v1/auth/login";
    string logoutUrl = "http://27.72.147.222:5011/api/v1/auth/logout";
    string refreshTokenUrl = "http://27.72.147.222:5011/api/v1/auth/refresh";

    public string userName = "";
    public string pwrd = "";

    public string access_token = "";
    public string refresh_token = "";

    public bool logedIn = false;

    // Start is called before the first frame update
    void Start()
    {
        uIManager = FindObjectOfType<UIManager>();

        _DefaultValue();

        logedIn = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator _Login()
    {
        LoginData loginData = new LoginData
        {
            username = userName,
            password = pwrd
        };

        string loginDataJson = JsonUtility.ToJson(loginData);

        UnityWebRequest webRequest = UnityWebRequest.Post(loginUrl, loginDataJson);
        webRequest.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(loginDataJson));
        webRequest.SetRequestHeader("Content-Type", "application/json");
        webRequest.method = UnityWebRequest.kHttpVerbPOST;

        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log("www.error - " + webRequest.error + "_Post");
        }
        else
        {
            JSONObject json = new JSONObject(webRequest.downloadHandler.text);

            if (json["data"]["access_token"] != null)
            {
                access_token = json["data"]["access_token"].ToString().Replace("\"", "");
            }

            if (json["data"]["refresh_token"] != null)
            {
                refresh_token = json["data"]["refresh_token"].ToString().Replace("\"", "");
            }

            Debug.Log("access_token = " + access_token);
            Debug.Log("refresh_token = " + refresh_token);

            if (access_token != "")
            {
                logedIn = true;

                uIManager._CloseLoginScreen();
            }

            //Debug.Log(webRequest.downloadHandler.text);
        }

        yield break;
    }

    public void _LogoutCoroutine()
    {
        StartCoroutine(_Logout());
    }

    public IEnumerator _Logout()
    {
        UnityWebRequest webRequest = UnityWebRequest.Post(logoutUrl, "POST");
        webRequest.SetRequestHeader("Content-Type", "application/json");
        webRequest.SetRequestHeader("Authorization", "Bearer " + access_token);
        webRequest.method = UnityWebRequest.kHttpVerbDELETE;

        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log("www.error - " + webRequest.error + "_Delete");
        }
        else
        {
            Debug.Log(webRequest.downloadHandler.text);
        }

        logedIn = false;

        _DefaultValue();

        uIManager._OpenLoginScreen();

        yield break;
    }

    void _DefaultValue()
    {
        //userName = "";
        //pwrd = "";

        access_token = "";
        refresh_token = "";
    }

    public IEnumerator _Refresh()
    {
        UnityWebRequest webRequest = UnityWebRequest.Post(refreshTokenUrl, "Post");
        webRequest.SetRequestHeader("Content-Type", "application/json");
        webRequest.SetRequestHeader("Authorization", "Bearer " + refresh_token);
        webRequest.method = UnityWebRequest.kHttpVerbPOST;

        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log("www.error - " + webRequest.error + "_Post");
        }
        else
        {
            Debug.Log(webRequest.downloadHandler.text);
        }

        yield break;
    }
}
