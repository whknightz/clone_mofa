﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskbarButtonController : MonoBehaviour
{
    UIManager uIManager;

    public enum CLICK_BUTTON { None, Setting, Maintenance, Disoder, Event, Recipe, Trend, Function }

    public GameObject settingButton, settingButtonActive;
    public GameObject maintenanceButton, maintenanceButtonActive;
    public GameObject disoderButton, disoderButtonActive;
    public GameObject eventButton, eventButtonActive;
    public GameObject recipeButton, recipeButtonActive;
    public GameObject trendButton, trendButtonActive;
    public GameObject functionButton, functionButtonActive;

    public CLICK_BUTTON currentClick = CLICK_BUTTON.None;

    // Start is called before the first frame update
    void Start()
    {
        uIManager = FindObjectOfType<UIManager>();

        _OutTask();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void _OutTask()
    {
        _ActiveGreyButton();
        _DeactiveRedButton();

        currentClick = CLICK_BUTTON.None;
    }

    void _DeactiveOthertaskMenu()
    {
        uIManager._CloseAllTaskbarMainUI();
    }

    void _ActiveGreyButton()
    {
        settingButton.SetActive(true);
        maintenanceButton.SetActive(true);
        disoderButton.SetActive(true);
        eventButton.SetActive(true);
        recipeButton.SetActive(true);
        trendButton.SetActive(true);
        functionButton.SetActive(true);
    }

    void _DeactiveRedButton()
    {
        settingButtonActive.SetActive(false);
        maintenanceButtonActive.SetActive(false);
        disoderButtonActive.SetActive(false);
        eventButtonActive.SetActive(false);
        recipeButtonActive.SetActive(false);
        trendButtonActive.SetActive(false);
        functionButtonActive.SetActive(false);
    }

    public void _SettingButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Setting)
        {
            currentClick = CLICK_BUTTON.Setting;

            _ActiveGreyButton();
            _DeactiveRedButton();

            settingButton.SetActive(false);
            settingButtonActive.SetActive(true);

            _DeactiveOthertaskMenu();
            uIManager._OpenSettings();
        }
        else
        {
            _OutTask();

            uIManager._CloseSettings();
        }
    }

    public void _MaintenanceButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Maintenance)
        {
            currentClick = CLICK_BUTTON.Maintenance;

            _ActiveGreyButton();
            _DeactiveRedButton();

            maintenanceButton.SetActive(false);
            maintenanceButtonActive.SetActive(true);

            _DeactiveOthertaskMenu();
        }
        else
        {
            _OutTask();
        }
    }

    public void _DisoderButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Disoder)
        {
            currentClick = CLICK_BUTTON.Disoder;

            _ActiveGreyButton();
            _DeactiveRedButton();

            disoderButton.SetActive(false);
            disoderButtonActive.SetActive(true);

            _DeactiveOthertaskMenu();
        }
        else
        {
            _OutTask();
        }
    }

    public void _EventButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Event)
        {
            currentClick = CLICK_BUTTON.Event;

            _ActiveGreyButton();
            _DeactiveRedButton();

            eventButton.SetActive(false);
            eventButtonActive.SetActive(true);

            _DeactiveOthertaskMenu();
        }
        else
        {
            _OutTask();
        }
    }

    public void _RecipeButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Recipe)
        {
            currentClick = CLICK_BUTTON.Recipe;

            _ActiveGreyButton();
            _DeactiveRedButton();

            recipeButton.SetActive(false);
            recipeButtonActive.SetActive(true);

            _DeactiveOthertaskMenu();
        }
        else
        {
            _OutTask();
        }
    }

    public void _TrendButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Trend)
        {
            currentClick = CLICK_BUTTON.Trend;

            _ActiveGreyButton();
            _DeactiveRedButton();

            trendButton.SetActive(false);
            trendButtonActive.SetActive(true);

            _DeactiveOthertaskMenu();
        }
        else
        {
            _OutTask();
        }
    }

    public void _FunctionButtonButtonClick()
    {
        if (currentClick != CLICK_BUTTON.Function)
        {
            currentClick = CLICK_BUTTON.Function;

            _ActiveGreyButton();
            _DeactiveRedButton();

            functionButton.SetActive(false);
            functionButtonActive.SetActive(true);

            _DeactiveOthertaskMenu();
        }
        else
        {
            _OutTask();
        }
    }
}
