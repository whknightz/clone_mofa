﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Networking;
//using System.Text;

[System.Serializable]
public class LoginData
{
    public string username = "";
    public string password = "";
}

public class Login : MonoBehaviour
{
    HTTPWebRequest httpWebRequest;

    public UIInput usernameInputField;
    public UIInput passwordInputField;

    public bool login = false;
    public bool logout = false;
    public bool refresh = false;

    // Start is called before the first frame update
    void Start()
    {
        httpWebRequest = FindObjectOfType<HTTPWebRequest>();

        usernameInputField.text = "test";
        passwordInputField.text = "admin@1234";
    }

    // Update is called once per frame
    void Update()
    {
        _GetUserNameAndPassword();

        if (login == true)
        {
            login = false;

            StartCoroutine(httpWebRequest._Login());
        }

        if(logout == true)
        {
            logout = false;

            StartCoroutine(httpWebRequest._Logout());
        }

        if (refresh == true)
        {
            refresh = false;

            StartCoroutine(httpWebRequest._Refresh());
        }
    }

    void _GetUserNameAndPassword()
    {
        httpWebRequest.userName = usernameInputField.text;
        httpWebRequest.pwrd = passwordInputField.text;
    }

    public void _SigninButton()
    {
        login = true;
    }
}
