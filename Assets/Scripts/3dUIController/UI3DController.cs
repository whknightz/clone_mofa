﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI3DController : MonoBehaviour
{
    public GameObject blackBG;
    public GameObject go3d;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _BlackBG();
    }

    void _BlackBG()
    {
        if (blackBG.activeSelf == true && go3d.activeSelf == false)
        {
            blackBG.SetActive(false);
        }
        if (blackBG.activeSelf == false && go3d.activeSelf == true)
        {
            blackBG.SetActive(true);
        }
    }
}
