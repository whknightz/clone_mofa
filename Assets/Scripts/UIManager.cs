﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    HTTPWebRequest hTTPWebRequest;

    public GameObject GO3d;
    public Login login;
    public TaskbarButtonController taskbarButtonController;
    public SettingController settingController;

    // Start is called before the first frame update
    void Start()
    {
        hTTPWebRequest = FindObjectOfType<HTTPWebRequest>();

        _Close3d();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void _CloseLoginScreen()
    {
        login.gameObject.SetActive(false);
        _CloseAllTaskbarMainUI();
        _OpenTaskBar();
        _Open3d();
    }
    public void _OpenLoginScreen()
    {
        login.gameObject.SetActive(true);
        _CloseAllTaskbarMainUI();
        _CloseTaskBar();
        _Close3d();
    }

    public void _Open3d()
    {
        GO3d.SetActive(true);
    }
    public void _Close3d()
    {
        GO3d.SetActive(false);
    }

    public void _CloseTaskBar()
    {
        taskbarButtonController.gameObject.SetActive(false);
    }
    public void _OpenTaskBar()
    {
        taskbarButtonController.gameObject.SetActive(true);
    }

    public void _CloseAllTaskbarMainUI()
    {
        settingController.gameObject.SetActive(false);

        _Open3d();
    }

    public void _OpenSettings()
    {
        _CloseAllTaskbarMainUI();
        _Close3d();
        settingController.gameObject.SetActive(true);
    }
    public void _CloseSettings()
    {
        _Open3d();
        settingController.gameObject.SetActive(false);
    }
}
