﻿using UnityEngine;

public class KNManager : MonoBehaviour
{
    public static GameObject go3D;
    public static GameObject login;
    public static GameObject taskbar;
    public static GameObject setting;

    public static KNSelectObject selectObject;

    void Awake()
    {
        selectObject = GameObject.FindObjectOfType<KNSelectObject>();
        selectObject.enabled = false;
        go3D = GameObject.Find("MoFa");
        go3D.SetActive(false);
        login = GameObject.Find("Login");
        login.SetActive(true);
        taskbar = GameObject.Find("TaskBarButtons");
        taskbar.SetActive(false);
        setting = GameObject.Find("Setting");
        setting.SetActive(false);
    }

    public static void LoginChangeUI()
    {
        go3D.SetActive(true);
        login.SetActive(false);
        taskbar.SetActive(true);
        selectObject.enabled = true;
    }

}
