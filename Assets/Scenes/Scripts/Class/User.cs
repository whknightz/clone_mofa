﻿using System;

[Serializable]
public class User
{
    public string avatar_path;
    public int create_date;
    public string creator_id;
    public int deactivate_user;
    public int edit_date;
    public string editor_id;
    public int first_login;
    public string first_name;
    public string group_id;
    public string id;
    public string language_id;
    public string last_name;
    public int level;
    public string password;
    public int sex;
    public string username;
}

[Serializable]
public class RootObject
{
    public User[] users;
}