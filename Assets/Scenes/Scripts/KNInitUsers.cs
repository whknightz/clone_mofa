﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KNInitUsers : MonoBehaviour
{
    [SerializeField] KNHttpWebRequest knHttpWebRequest;

    public static GameObject grid;
    public static User[] users;
    public static GameObject userButton;

    void Awake()
    {
        userButton = Resources.Load<GameObject>("Prefabs/UserButton");
        grid = gameObject;
    }

    void Start()
    {
        StartCoroutine(knHttpWebRequest.GetAllUser());
    }

    public static void Show()
    {
        for (int i = 0; i < users.Length; i++)
        {
            GameObject obj = Instantiate(userButton, grid.transform);
            obj.GetComponentInChildren<UILabel>().text = users[i].username;
        }
        grid.GetComponent<UIGrid>().Reposition();
    }

}
