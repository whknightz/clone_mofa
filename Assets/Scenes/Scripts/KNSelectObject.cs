﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KNSelectObject : MonoBehaviour
{
    private Camera cam;
    private GameObject miniGO;
    private int t;

    void Start()
    {
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        if (miniGO != null && t < 10)
        {
            t++;
            float f = t / 10f;
            miniGO.transform.localScale = new Vector3(f, f, f);
        }
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            bool hit = Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
                if (miniGO != null)
                    Destroy(miniGO);
                miniGO = Instantiate(hitInfo.collider.gameObject, new Vector3(-5.5f, 1f, 0f), Quaternion.Euler(-90f,0f,0f));
                Destroy(miniGO.GetComponent<MeshCollider>());
                miniGO.transform.localScale = Vector3.zero;
                t = 0;
                Debug.Log("Hit " + hitInfo.transform.gameObject.name);
            }
            //else
            //{
            //    Debug.Log("No hit");
            //}
        }
    }
}
