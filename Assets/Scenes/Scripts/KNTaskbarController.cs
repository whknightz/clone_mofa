﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KNTaskbarController : MonoBehaviour
{
    public delegate void Delegate();
    public Delegate[] FuncClose;

    public GameObject[] buttonOff;
    public GameObject[] buttonOn;
    public int current = -1;

    private KNSelectObject selectObject;

    void Start()
    {
        FuncClose = new Delegate[] { CloseSetting, CloseMaintenance, CloseDisoder, CloseEvent, CloseRecipe, CloseTrend, CloseFunctionsGroup };

        selectObject = KNManager.selectObject;

        foreach (GameObject go in buttonOn)
        {
            go.SetActive(false);
        }
    }

    public void Click(int id)
    {
        if (current == -1)
        {
            current = id;
            buttonOff[current].SetActive(false);
            buttonOn[current].SetActive(true);
            selectObject.enabled = false;
        }
        else if (current == id)
        {
            buttonOff[current].SetActive(true);
            buttonOn[current].SetActive(false);
            current = -1;
            selectObject.enabled = true;
        }
        else
        {
            FuncClose[current]();
            buttonOff[current].SetActive(true);
            buttonOn[current].SetActive(false);
            current = id;
            buttonOn[current].SetActive(true);
            buttonOff[current].SetActive(false);
            selectObject.enabled = false;
        }
    }

    public void SettingButton()
    {
        Click(0);
        if (current == 0)
            KNManager.setting.SetActive(true);
        else
            KNManager.setting.SetActive(false);
    }

    public void MaintenanceButton()
    {
        Click(1);
    }

    public void DisoderButton()
    {
        Click(2);
    }

    public void EventButton()
    {
        Click(3);
    }

    public void RecipeButton()
    {
        Click(4);
    }

    public void TrendButton()
    {
        Click(5);
    }

    public void FunctionsGroupButton()
    {
        Click(6);
    }

    public void CloseSetting()
    {
        KNManager.setting.SetActive(false);
    }

    public void CloseMaintenance()
    {

    }

    public void CloseDisoder()
    {

    }

    public void CloseEvent()
    {

    }

    public void CloseRecipe()
    {

    }

    public void CloseTrend()
    {

    }

    public void CloseFunctionsGroup()
    {

    }
}
