﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KNLoginData
{
    public string username = "";
    public string password = "";
}

public class KNLogin : MonoBehaviour
{
    [SerializeField] KNHttpWebRequest knHttpWebRequest;

    [SerializeField] UIInput txtUsername;
    [SerializeField] UIInput txtPassword;

    void Start()
    {

    }

    void Update()
    {

    }

    public void Login()
    {
        knHttpWebRequest.SetUserPwd(txtUsername.value, txtPassword.value);
        StartCoroutine(knHttpWebRequest.Login());
    }
}
