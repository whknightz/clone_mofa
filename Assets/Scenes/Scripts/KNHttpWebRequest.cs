﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class KNHttpWebRequest : MonoBehaviour
{
    string loginUrl = "http://27.72.147.222:5011/api/v1/auth/login";
    string logoutUrl = "http://27.72.147.222:5011/api/v1/auth/logout";
    string refreshTokenUrl = "http://27.72.147.222:5011/api/v1/auth/refresh";

    string apiGetAllUsers = "http://27.72.147.222:5011/api/v1/users";

    public string username = "test";
    public string password = "admin@1234";

    public string access_token = "";
    public string refresh_token = "";

    public IEnumerator GetAllUser()
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(apiGetAllUsers);
        webRequest.SetRequestHeader("Content-Type", "application/json");
        webRequest.SetRequestHeader("Authorization", "Bearer " + access_token);
        webRequest.method = UnityWebRequest.kHttpVerbGET;
        yield return webRequest.SendWebRequest();
        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log("www.error - " + webRequest.error + "_Post");
        }
        else
        {
            JSONObject json = new JSONObject(webRequest.downloadHandler.text);
            RootObject users = JsonUtility.FromJson<RootObject>("{\"users\": " + json["data"].ToString() + "}");
            KNInitUsers.users = users.users;
            KNInitUsers.Show();
        }
        yield break;
    }

    public void SetUserPwd(string username, string password)
    {
        this.username = username;
        this.password = password;
    }

    public IEnumerator Login()
    {
        KNLoginData loginData = new KNLoginData
        {
            username = username,
            password = password
        };

        string loginDataJson = JsonUtility.ToJson(loginData);

        UnityWebRequest webRequest = UnityWebRequest.Post(loginUrl, loginDataJson);
        webRequest.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(loginDataJson));
        webRequest.SetRequestHeader("Content-Type", "application/json");
        webRequest.method = UnityWebRequest.kHttpVerbPOST;

        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log("www.error - " + webRequest.error + "_Post");
        }
        else
        {
            JSONObject json = new JSONObject(webRequest.downloadHandler.text);

            if (json["data"]["access_token"] != null)
            {
                access_token = json["data"]["access_token"].ToString().Replace("\"", "");
            }

            if (json["data"]["refresh_token"] != null)
            {
                refresh_token = json["data"]["refresh_token"].ToString().Replace("\"", "");
            }

            Debug.Log("access_token = " + access_token);
            Debug.Log("refresh_token = " + refresh_token);

            if (access_token != "")
            {
                KNManager.LoginChangeUI();
            }
        }

        yield break;
    }
}
